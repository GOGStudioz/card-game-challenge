﻿/// <summary>
/// This constant class holds all the game data constants. 
/// </summary>
/// 
public class GameConstant
{
	public enum GameType
	{
		TwoPlayerGame,
		FourPlayerGame}
	;

	public enum CardType
	{
		Club,
		Diamond,
		Heart,
		Spade}
	;

	public enum GameStates
	{
		Shuffling,
		GameOn,
		War,
	}

	public const int cardsInPack = 13;
	public const int packCount = 52;
	public const string gameSceneName = "GameScene";

}
