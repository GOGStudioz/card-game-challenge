﻿/// <summary>
/// Message constants to be displayed on game play
/// </summary>
public class MessageConstants
{
	public const string PlayMessage = "Tap on your respective cards to play the turn";
	public const string GameWonText = " has won this turn";
	public const string WarText = " cards are placed in this bet. Winner takes it all.";
	public const string matchWonText=" has won the match. Wanna player again? Press restart.";
}
