﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Cards data manager is responsible for all the business logic included in the cards manupulation.
/// </summary>
public class CardsDataManager
{
	public Action<int> resultDeclaredEvent;
	public Action<int> warDeclaredEvent;
	public Action<int> gameCompletedEvent;

	public bool isWarOn;

	List<int> allCards = new List<int> ();

	PlayerDataManager playerDataManager;
	static CardsDataManager instance;

	CardModel[] turnData = new CardModel[PlayerDataManager.GetInstance ().GetTotalPlayers];
	CardModel[] warData;

	int turnsPlayed;

	private CardsDataManager ()
	{
		playerDataManager = PlayerDataManager.GetInstance ();
	}

	/// <summary>
	/// Gets the singleton instance of CardsDataManager class.
	/// </summary>
	/// <returns>The instance.</returns>
	public static CardsDataManager GetInstance ()
	{    
		if (instance == null)
			instance = new CardsDataManager ();		
		return instance;
	}

	/// <summary>
	/// Creates the cards list.
	/// </summary>
	public void CreateCardsList ()
	{
		turnsPlayed = 0;
		isWarOn = false;
		for (int i = 0; i < GameConstant.packCount; i++)
			allCards.Add (i + 1);		
		ShuffleAndDistributeCard ();
	}

	/// <summary>
	/// This will help shuffling and distributing all the existing cards.
	/// </summary>
	void ShuffleAndDistributeCard ()
	{
		int totalCard = allCards.Count;
		while (totalCard > 0) {
			int index =	UnityEngine.Random.Range (0, totalCard);
			int item = allCards [index];
			int playerIndex = item % playerDataManager.GetTotalPlayers;
			playerDataManager.players [playerIndex].cards.Enqueue (new CardModel (item));
			allCards.Remove (item);
			totalCard--;
		}
		Debug.Log ("Shuffle card breakpoint " + allCards.Count);
	}

	/// <summary>
	/// User the played a turn. Store the card data and check if both players have played their turn then check for winner.
	/// </summary>
	/// <param name="cardModel">Card model.</param>
	/// <param name="userIndex">User index.</param>
	public void UserPlayedTurn (CardModel cardModel, int userIndex)
	{
		turnData [userIndex - 1] = cardModel;
		turnsPlayed++;
		if (playerDataManager.GetTotalPlayers == turnsPlayed) {
			CheckForWinner ();
		}
	}

	/// <summary>
	/// Compare the played card of both the user and check if there is a war situation or any of the user won the turn
	/// </summary>
	void CheckForWinner ()
	{
		// if the cards are of same weightage then it's war
		if (turnData [0].valueAssigned == turnData [1].valueAssigned) {
			int gameWinnerIndex = CheckForGameCompletion ();
			if (gameWinnerIndex > 0) {
				ConsumeTurnData (gameWinnerIndex);
				return;
			}
	
			int betSize = CheckForBetSize (turnData [0].valueAssigned);
			//If it's a back to back war case.
			if (isWarOn) {
				int totalBet =	(betSize * 2) + turnsPlayed + warData.Length;
				CardModel[] tempData = new CardModel[warData.Length];
				warData.CopyTo (tempData, 0);
				warData = new CardModel[totalBet];
				tempData.CopyTo (warData, totalBet - tempData.Length);
				tempData = null;
			} else {
				isWarOn = true;
				int totalBet =	(betSize * 2) + turnsPlayed;
				warData = new CardModel[totalBet];
			}
			FetchCardsInCaseOfWar (betSize);
			turnsPlayed = 0;
		} else {
			int turnWinnerIndex = 0;
			if (turnData [0].valueAssigned > turnData [1].valueAssigned)
				turnWinnerIndex = 0;
			else
				turnWinnerIndex = 1;
			
			ConsumeTurnData (turnWinnerIndex);
			ResetTurnData ();
			if (CheckForGameCompletion () > 0)
				return;
			
			//intimate UI Controller the winner is declared
			if (resultDeclaredEvent != null)
				resultDeclaredEvent (turnWinnerIndex);
		}
	}

	#region WAR Related logic

	/// <summary>
	/// If any of user has lesser card than the bet itself then
	/// we change the value of bet to the  (least available card count -1), leave one card to show. 
	/// </summary>
	int CheckForBetSize (int betSize)
	{
		// check for the betSize based on cards availability
		foreach (PlayerModel player in playerDataManager.players) {
			if (player.cards.Count < betSize + 1) {
				betSize = player.cards.Count - 1;
				break;
			}
		}
		return betSize;
	}

	/// <summary>
	/// Fetchs the cards from each user's queue based on availability. 
	/// </summary>
	/// <param name="betSize">Bet size.</param>
	void FetchCardsInCaseOfWar (int betSize)
	{		
		// Store the bet cards in the warData
		foreach (PlayerModel player in playerDataManager.players) {
			for (int i = 0; i < betSize; i++) {
				warData [i + (betSize * (player.userIndex - 1))] = player.cards.Dequeue ();
			}
		}

		int totalBet = (betSize * 2);
		// add turn data , change this to make it support multiple players
		warData [totalBet] = turnData [0];	
		warData [totalBet + 1] = turnData [1];	

		if (warDeclaredEvent != null)
			warDeclaredEvent (warData.Length);

	}

	#endregion //War Related logic


	/// <summary>
	/// Winner consumes the card placed in existing bet.
	/// </summary>
	/// <param name="winnerIndex">Winner index.</param>
	void ConsumeTurnData (int winnerIndex)
	{
		AwardWinner (turnData, winnerIndex);
		if (isWarOn)
			AwardWinner (warData, winnerIndex);
	}


	/// <summary>
	/// Award the winner i.e add the winning cards in winner's queue.
	/// </summary>
	/// <param name="winningAmount">Winning amount.</param>
	/// <param name="playerIndex">Player index.</param>
	void AwardWinner (CardModel[] winningData, int playerIndex)
	{
		for (int i = 0; i < winningData.Length; i++) {
			CardModel cardModel = winningData [i];
			if (cardModel != null)
				playerDataManager.players [playerIndex].cards.Enqueue (cardModel);
			else
				return;
		}
	}


	/// <summary>
	///Check if all the playing user has atleast 1 card in hand after this turn.
	/// </summary>
	int CheckForGameCompletion ()
	{		
		int winnerIndex = -1;
		// check for the betSize based on cards availability
		foreach (PlayerModel player in playerDataManager.players) {
			if (player.cards.Count == 0) {
				if (player.userIndex == 1)
					winnerIndex = 2;
				else
					winnerIndex = 1;

				playerDataManager.playerWon = playerDataManager.players [winnerIndex - 1];
				ResetTurnData ();
				gameCompletedEvent (winnerIndex);
				break;
			}
		}
		return winnerIndex;
	}


	/// <summary>
	/// Resets the turn related data.
	/// </summary>
	void ResetTurnData ()
	{
		isWarOn = false;	
		turnsPlayed = 0;
	}

}
