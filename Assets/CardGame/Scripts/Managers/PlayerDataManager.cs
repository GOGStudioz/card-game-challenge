﻿
using System;
using System.Collections.Generic;

/// <summary>
/// This manager class is renposible to hold all the players related session information and perform
///  all the business operation required on gameplay.
/// </summary>
/// 
public class PlayerDataManager
{
	public List<PlayerModel> players = new List<PlayerModel> ();
	public GameConstant.GameType currentGameType = GameConstant.GameType.TwoPlayerGame;

	public PlayerModel playerWon;

	static PlayerDataManager instance;

	private PlayerDataManager ()
	{
	}

	/// <summary>
	/// Gets the singleton instance of PlayerDataManager class.
	/// </summary>
	/// <returns>The instance.</returns>
	public static PlayerDataManager GetInstance ()
	{    
		if (instance == null)
			instance = new PlayerDataManager ();		
		return instance;
	}

	/// <summary>
	/// Returns the total player count in the game ,based on game type choosen.
	/// </summary>
	/// <value>The get total players.</value>
	public int GetTotalPlayers {
		get { 
			int playerCount = 0;
			switch (currentGameType) {
			case GameConstant.GameType.TwoPlayerGame:
				playerCount = 2;
				break;
			case GameConstant.GameType.FourPlayerGame:
				playerCount = 4;
				break;
			}
			return playerCount;
		}
	}


}
