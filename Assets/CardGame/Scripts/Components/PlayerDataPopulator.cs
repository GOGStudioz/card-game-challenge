﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// This component is responsible to populate and manipulate the single player reltaed data in the gameplay UI.
/// </summary>
public class PlayerDataPopulator : MonoBehaviour
{
	[HideInInspector]
	public bool isTurnPlayed;

	[SerializeField]
	Text userName, cardsLeft;
	[SerializeField]
	int userIndex;
	[SerializeField]
	CardPrefabUIController tempHolder, cardHolder, cardPlayed;

	PlayerDataManager gameDataManager;
	PlayerModel player;


	// Use this for initialization
	void Awake ()
	{
		gameDataManager = PlayerDataManager.GetInstance ();
		player = gameDataManager.players [userIndex];
		SetData ();
	}

	/// <summary>
	/// Sets the data in the game UI corresponding to a single player.
	/// </summary>
	public void SetData ()
	{
		isTurnPlayed = false;
		userName.text = player.name;
		cardsLeft.text = "Cards Left " + player.cards.Count;
	}

	/// <summary>
	/// Actives the cards in the game UI for a single player.
	/// </summary>
	/// <param name="isCardHolderActive">If set to <c>true</c> is card holder active.</param>
	/// <param name="isCardPlayedActive">If set to <c>true</c> is card played active.</param>
	/// <param name="isTempHolderActive">If set to <c>true</c> is temp holder active.</param>
	public void ActiveCards (bool isCardHolderActive, bool isCardPlayedActive)
	{
		cardHolder.gameObject.SetActive (isCardHolderActive);
		cardPlayed.gameObject.SetActive (isCardPlayedActive);
		if (CardsDataManager.GetInstance ().isWarOn)
			tempHolder.gameObject.SetActive (true);
		else
			tempHolder.gameObject.SetActive (false);	
	}

	/// <summary>
	/// User will play its turn by clicking on the card holder section
	/// </summary>
	public void PlayTurn ()
	{
		if (!isTurnPlayed && GameplayUIController.currentGameState == GameConstant.GameStates.GameOn) {
			isTurnPlayed = true;
			cardPlayed.gameObject.SetActive (true);
			CardModel cardModel = player.cards.Dequeue ();
			this.cardPlayed.SetData (cardModel, false);
			CardsDataManager.GetInstance ().UserPlayedTurn (cardModel, this.player.userIndex);
			if (player.cards.Count == 0)
				ActiveCards (false, true);
		}
	}

}
