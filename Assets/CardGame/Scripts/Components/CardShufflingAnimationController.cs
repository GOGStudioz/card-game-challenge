﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// This component is attached to the animation game object and is responsible to perform action based on animation callbacks.
/// </summary>
public class CardShufflingAnimationController : MonoBehaviour
{
	public Action shufflingDone;
	public Action<int> ShowCard;

	int totalShuffleRequired = 0;
	int loopsCompleted = 0;

	// Use this for initialization
	void Awake ()
	{
//		totalShuffleRequired = (GameConstant.packCount / 2) / PlayerDataManager.GetInstance ().GetTotalPlayers;

		// Reduced from shuffle cycle from 26(2 players) to 8 to save some time for testing. 
		totalShuffleRequired = 4;
	}

	/// <summary>
	/// First half of the animation is completed activate left side card holder.
	/// </summary>
	void FirstHalfCompleted ()
	{
		if (loopsCompleted == 0 && ShowCard != null) {
			ShowCard (1);
		}
	}

	/// <summary>
	/// Seconds half is completed activate right side card holder.
	/// </summary>
	void  SecondHalfCompleted ()
	{
		if (loopsCompleted == 0 && ShowCard != null) {
			ShowCard (2);
		}
	}

	/// <summary>
	/// One animation loop is completed
	/// </summary>
	void LoopCompleted ()
	{
		loopsCompleted++;
		if (loopsCompleted == totalShuffleRequired && shufflingDone != null) {
			shufflingDone ();
			this.gameObject.SetActive (false);
		}
	}

}
