﻿using System;
using System.Collections.Generic;

/// <summary>
/// This is plain data holder class for player.
/// </summary>
public class PlayerModel
{
	string _name = "";

	// If user name has not been set up, provide a guest username.
	public string name {		
		get { 
			return _name;
		}
		set { 
			if (value.Equals ("")) {
				_name = "Guest_" + userIndex;
			} else {
				_name = value;
			}
		}
	}
	// Card holding DS for each player
	public Queue<CardModel> cards = new Queue<CardModel> ();
	// UserIndex is kept to track , if it's user 1,2..4
	public int userIndex;
}
