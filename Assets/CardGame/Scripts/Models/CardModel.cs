﻿using System;
using UnityEngine;

/// <summary>
/// This is plain data holder class for playing card.
/// </summary>
public class CardModel
{
	public int valueAssigned;
	public GameConstant.CardType cardType;

	public CardModel (int index)
	{
		SetIndex (index);
	}

	/// <summary>
	/// This will set up base information of card an index of 1-52 , will decide the value and the type of card.
	/// </summary>
	/// <param name="index">Index.</param>
	void SetIndex (int index)
	{
		valueAssigned = index % GameConstant.cardsInPack;
		if (valueAssigned == 0)
			valueAssigned = 13;
		float cardIndex = (index - 1) / GameConstant.cardsInPack;
		cardType = (GameConstant.CardType)Mathf.Floor (cardIndex);
		Debug.Log (cardType);
	}

}
