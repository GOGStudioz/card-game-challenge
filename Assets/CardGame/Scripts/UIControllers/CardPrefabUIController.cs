﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardPrefabUIController : MonoBehaviour
{
	public CardModel cardModel;

	[SerializeField]
	Sprite[] cardTypeIcon;
	[SerializeField]
	Image cardIcon;
	[SerializeField]
	Text cardIndex;
	[SerializeField]
	GameObject downFace;

	// Use this for initialization
	public void SetData (CardModel model, bool isDownFace)
	{
		if (!isDownFace) {
			this.cardModel = model;
			cardIcon.sprite = cardTypeIcon [(int)model.cardType];
			cardIndex.text = "" + model.valueAssigned;
		} else {
			downFace.SetActive (true);
		}
	}

}
