﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This UI controller is responsible to generate name input fields based on number of user's supported
/// </summary>

public class NameSelectionPanelUiController : MonoBehaviour
{

	[SerializeField]
	Transform parentTransform;

	[SerializeField]
	GameObject namePrefabs;

	PlayerDataManager gameDataManager;

	// Use this for initialization
	void Awake ()
	{
		gameDataManager = PlayerDataManager.GetInstance ();
		PopulateNameSection (gameDataManager.GetTotalPlayers);
	}

	/// <summary>
	/// Intantiate nameing input fields depanding upon game type and intializing user so that 
	/// the properties can be user in the later half of the project
	/// </summary>
	/// <param name="totalCount">Total count.</param>
	void PopulateNameSection (int totalCount)
	{
		for (int i = 0; i < totalCount; i++) {
			GameObject prefab = Instantiate (namePrefabs, parentTransform);
			PlayerModel playerModel = new PlayerModel ();
			playerModel.userIndex = i + 1;
			prefab.GetComponent<NameEntryPrefabController> ().SetData (playerModel);
			// Game supports only 2 players for now.
			if (gameDataManager.players.Count < 2)
				gameDataManager.players.Add (playerModel);
		}
	}

	/// <summary>
	/// Submits the names.
	/// </summary>
	public void SubmitNames ()
	{
		PlayerDataManager.GetInstance ().currentGameType = GameConstant.GameType.TwoPlayerGame;
		SceneManager.LoadScene (GameConstant.gameSceneName);
	}
}
