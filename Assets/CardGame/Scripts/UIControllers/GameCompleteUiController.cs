﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Game complete user interface controller.
/// </summary>
public class GameCompleteUiController : MonoBehaviour
{
	[SerializeField]
	Text userWonText = null;
	[SerializeField]
	GameObject gameScreenPanel;
	PlayerDataManager playerdataManager;

	void Awake ()
	{
		playerdataManager = PlayerDataManager.GetInstance ();
	}

	// Use this for initialization
	void OnEnable ()
	{
		userWonText.text = playerdataManager.playerWon.name + MessageConstants.matchWonText;	
	}

	/// <summary>
	/// Restart the game.
	/// </summary>
	public void Restart ()
	{
		Instantiate (gameScreenPanel, transform.parent);
		Destroy (this.gameObject);
	}

	/// <summary>
	/// Move to Menu Screen.
	/// </summary>
	public void Home ()
	{
		SceneManager.LoadScene ("MenuScene");
	}
}
