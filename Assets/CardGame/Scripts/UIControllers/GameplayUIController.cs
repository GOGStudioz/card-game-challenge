﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Main UI Class of gameplay, Which will pass information between other UI controller and,
///  if reuqired interact with the managers.
/// </summary>
public class GameplayUIController : MonoBehaviour
{
	public static GameConstant.GameStates currentGameState;

	[SerializeField]
	CardShufflingAnimationController centerCardController;
	[SerializeField]
	PlayerDataPopulator[] playerPopulator;
	[SerializeField]
	Text commonInformationText;
	[SerializeField]
	GameObject gameoverScreen;

	CardsDataManager cardDataManager;
	PlayerDataManager playerDataManager;

	#region Mono lifecycle

	// Use this for initialization
	void Awake ()
	{
		currentGameState = GameConstant.GameStates.Shuffling;
		cardDataManager = CardsDataManager.GetInstance ();
		playerDataManager = PlayerDataManager.GetInstance ();
		cardDataManager.CreateCardsList ();
	}

	void OnEnable ()
	{
		centerCardController.shufflingDone += UpdateGameStats;
		centerCardController.ShowCard += ShowPlayerCard;
		cardDataManager.resultDeclaredEvent += OnReultDeclared;
		cardDataManager.warDeclaredEvent += OnWarDeclared;
		cardDataManager.gameCompletedEvent += OnGameCompleted;			
	}

	void OnDisable ()
	{
		centerCardController.shufflingDone -= UpdateGameStats;
		centerCardController.ShowCard -= ShowPlayerCard;
		cardDataManager.resultDeclaredEvent -= OnReultDeclared;
		cardDataManager.warDeclaredEvent -= OnWarDeclared;
		cardDataManager.gameCompletedEvent -= OnGameCompleted;
	}

	#endregion // mono lifecycle end

	/// <summary>
	/// Shows the player card syncing with animation.
	/// </summary>
	/// <param name="playerIndex">Player index.</param>
	void ShowPlayerCard (int playerIndex)
	{
		playerPopulator [playerIndex - 1].ActiveCards (true, false);
	}

	/// <summary>
	/// Shufflings complete callback from animation.
	/// </summary>
	void UpdateGameStats ()
	{		
		currentGameState = GameConstant.GameStates.GameOn;
		commonInformationText.text = MessageConstants.PlayMessage;
		foreach (PlayerDataPopulator dataPopulator in playerPopulator) {
			dataPopulator.SetData ();
			dataPopulator.ActiveCards (true, false);
		}
		// Show message to start the gameplay.
	}

	#region CardManager's callbacks.

	/// <summary>
	/// Callback received when a result is declared.
	/// </summary>
	/// <param name="winnerIndex">Winner index.</param>
	void OnReultDeclared (int winnerIndex)
	{
		commonInformationText.text = playerDataManager.players [winnerIndex].name + MessageConstants.GameWonText;
		Invoke ("UpdateGameStats", 3);
	}

	/// <summary>
	/// Callback received when a war is declared.
	/// </summary>
	/// <param name="betSize">Bet size.</param>
	void OnWarDeclared (int betSize)
	{
		commonInformationText.text = betSize + MessageConstants.WarText;
		Invoke ("UpdateGameStats", 7);
	}

	/// <summary>
	/// Callback received when a game is completed.
	/// </summary>
	/// <param name=" index">Game winner index .</param>
	void OnGameCompleted (int index)
	{
		Instantiate (gameoverScreen, transform.parent);
		Destroy (this.gameObject);
	}

	#endregion
}
