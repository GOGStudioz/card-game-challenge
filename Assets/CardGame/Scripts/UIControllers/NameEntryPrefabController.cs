﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Name entry prefab controller is responsible to set initial state of  user based on the data filled.
/// </summary>
public class NameEntryPrefabController : MonoBehaviour
{
	[SerializeField]
	Text userCount = null, userName = null;
	[SerializeField]
	InputField inputField;
	PlayerModel player;

	/// <summary>
	/// Sets the dynamic data for the prefab.
	/// </summary>
	/// <param name="playerModel">Player model.</param>
	public void SetData (PlayerModel playerModel)
	{
		gameObject.name = "" + playerModel.userIndex;
		userCount.text = "Player " + playerModel.userIndex;
		this.player = playerModel;
		Debug.Log ("This palyer name is " + this.player.name);
	}

	/// <summary>
	/// Set the name of the user when the UI is closed. If nothing is entered user should be given a default username.
	/// </summary>
	public void OnDisable ()
	{
		this.player.name = inputField.text;
	}
}
