﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUIController : MonoBehaviour
{
	[SerializeField]
	GameObject startGameButton, playerSelectionPanel, nameSelectionPanel;

	void OnEnable ()
	{
		startGameButton.SetActive (true);
		playerSelectionPanel.SetActive (false);
		ChangeButtonState (true, false);
	}

	/// <summary>
	/// Starts the game.
	/// </summary>
	public void StartGame ()
	{
		ChangeButtonState (false, true);
	}

	/// <summary>
	/// Selects the number of players.
	/// </summary>
	/// <param name="gameType">Game type.</param>
	public void SelectPlayer (int gameType)
	{
		ChangeButtonState (false, false);
		PlayerDataManager.GetInstance ().currentGameType = (GameConstant.GameType)gameType;
		nameSelectionPanel.SetActive (true);
	}

	/// <summary>
	/// Changes the state of the button in the menu panel
	/// </summary>
	/// <param name="isStartGameActive">If set to <c>true</c> is start game active.</param>
	/// <param name="isPlayerSelectionActive">If set to <c>true</c> is player selection active.</param>
	void ChangeButtonState (bool isStartGameActive, bool isPlayerSelectionActive)
	{
		startGameButton.SetActive (isStartGameActive);
		playerSelectionPanel.SetActive (isPlayerSelectionActive);
	}

}
